package dev.loredb;

import org.junit.Before;

public class InMemoryBooksDBTest extends AbstractBooksDBTest {

    @Before
    public void setUp() {
        db = LoreDB.inMemory();
        populateRecords();
    }

}
