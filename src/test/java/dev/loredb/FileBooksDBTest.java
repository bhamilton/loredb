package dev.loredb;

import dev.loredb.io.IORuntimeException;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileBooksDBTest extends AbstractBooksDBTest {

    private static final String PATH = "testdb";

    @Before
    public void setUp() {
        db = LoreDB.fromPath(PATH);
        populateRecords();
    }

    @After
    public void tearDown() throws Exception {
        Path dir = Paths.get("testdb");
        Files.list(dir).forEach(this::deleteFile);
        Files.delete(dir);
    }

    private void deleteFile(Path path) {
        try {
            if (Files.isDirectory(path))
                Files.list(path).forEach(this::deleteFile);
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

}
