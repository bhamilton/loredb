package dev.loredb;

import dev.loredb.Change.Insert;
import dev.loredb.criteria.Criteria;
import dev.loredb.io.JsonOptions;
import dev.loredb.io.JsonParser;
import dev.loredb.schema.JsonSchema;
import dev.loredb.schema.JsonSchemaException;
import lombok.Data;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static dev.loredb.criteria.Comparison.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class AbstractBooksDBTest {

    static final String BOOKS = "books";

    @Data
    public static class Book {
        public final String isbn;
        public final String title;
        public final String subtitle;
        public final List<String> authors;
        public final String published;
        public final String publisher;
        public final int pages;
        public final String description;
        public final String website;
    }

    LoreDB db;

    void populateRecords() {
        books().changeSchema(new JsonParser().parse(
                "                {\n" +
                "                  \"type\": \"object\",\n" +
                "                  \"properties\": {\n" +
                "                    \"isbn\": { \"type\": \"string\" },\n" +
                "                    \"title\": { \"type\": \"string\" },\n" +
                "                    \"subtitle\": { \"type\": \"string\" },\n" +
                "                    \"authors\": { \n" +
                "                        \"type\": \"array\",\n" +
                "                        \"items\": {\n" +
                "                           \"type\": \"string\"\n" +
                "                        } \n" +
                "                    },\n" +
                "                    \"published\": { \"type\": \"string\" },\n" +
                "                    \"publisher\": { \"type\": \"string\" },\n" +
                "                    \"pages\": { \"type\": \"number\" },\n" +
                "                    \"description\": { \"type\": \"string\" },\n" +
                "                    \"website\": { \"type\": \"string\" }\n" +
                "                  }\n" +
                "                }").asA(JsonSchema.class));

        books().insert((ArrayNode) JsonNode.fromResource("/books.json"));
    }

    @Test
    public void querying() {
        assertTitlesMatch("\"Programming JavaScript Applications\", \"Eloquent JavaScript, Second Edition\"",
                matches("title", ".*Javascript.*tions?"));
        assertTitlesMatch("\"Eloquent JavaScript, Second Edition\"",
                inBetween("pages", 400, 500).and(subsetOf("authors", asList("Pedro Félix", "Marijn Haverbeke"))));
        assertTitlesMatch("",
                noneOf("publisher", asList("O'Reilly Media", "No Starch Press")));
    }

    @Test
    public void reflection() {
        JsonNode node = books().select(eq("isbn", "9781491950296")).findFirst().orElseThrow();
        Book book = node.asA(Book.class);
        assertEquals(new Book(
                "9781491950296",
                "Programming JavaScript Applications",
                "Robust Web Architecture with Node, HTML5, and Modern JS Libraries",
                singletonList("Eric Elliott"),
                "2014-07-01T00:00:00.000Z",
                "O'Reilly Media",
                254,
                "Take advantage of JavaScript's power to build robust web-scale or enterprise applications that are " +
                        "easy to extend and maintain. By applying the design patterns outlined in this practical book, " +
                        "experienced JavaScript developers will learn how to write flexible and resilient code that's " +
                        "easier-yes, easier-to work with as your code base grows.",
                "http://chimera.labs.oreilly.com/books/1234000000262/index.html"
        ), book);
    }

    @Test
    public void update() {
        books().update(greaterThan("pages", 400), node -> node.assign("subtitle", "Big Ol' Book"));

        assertTitlesMatch("\"Designing Evolvable Web APIs with ASP.NET\", \"Speaking JavaScript\", \"Eloquent JavaScript, Second Edition\"",
                eq("subtitle", "Big Ol' Book").and(greaterThan("pages", 400)));
    }

    @Test
    public void delete() {
        books().delete(matches("title", ".*Javascript.*").or(matches("subtitle", ".*Javascript.*")));

        assertTitlesMatch("\"Designing Evolvable Web APIs with ASP.NET\", \"Git Pocket Guide\", \"You Don't Know JS\"");
    }

    @Test
    public void userAuth() {
        db.register("todd", "password123");
        assertEquals("todd", books().user().getName());
        db.authenticate("todd", "password123");

        verifyToddsAuth();
        db.authenticate(db.getUser().getNumber());
        int toddsId = db.getUser().getNumber();
        db.authenticate(0);
        db.authenticate(toddsId);
        verifyToddsAuth();
    }

    private void verifyToddsAuth() {
        books().update(EVERYTHING, n -> n.assign("title", "test"));
        assertEquals("todd shouldn't be able to modify records he doesn't own", 0L, books().select(eq("title", "test")).count());
        books().delete(matches("title", ".*Javascript.*").or(matches("subtitle", ".*Javascript.*")));
        assertEquals("todd shouldn't be able to modify records he doesn't own", 8L, books().selectAll().count());
    }

    @Test
    public void subscribe() {
        List<Change> changes = new ArrayList<>();
        books().subscribe(EVERYTHING, changes::add);
        books().update(matches("subtitle", ".*Javascript.*"), book -> book.assign("pages", 200));
        ObjectNode node = books().selectAll().findFirst().orElseThrow();
        books().insert(node);
        assertEquals(2, changes.size());
    }

    @Test(expected = JsonSchemaException.class)
    public void invalidInsert() {
        books().insert("{\"title\": 69, \"subtittie\": \"nice\"}");
    }

    @Test(expected = JsonSchemaException.class)
    public void updateInvalid() {
        books().update(EVERYTHING, n -> n.assign("subtittie", "nice"));
    }

    @Test
    public void insertAssignmentsDetailedJson() {
        books().insert("{\"title\":\"Test Book 1\"}");
        books().insert("{\"_id\":null,\"title\":\"Test Book 2\"}");
        books().select(matches("title", "Test Book.*")).forEach(row -> {
            String json = row.json(JsonOptions.DETAILED);
            assertTrue(json.matches("\\{\"_id\":\"[\\w=_-]+\",\"_creator\":0,\"_created\":\"....-..-..T..:..:..\\....Z\",\"title\":\"Test Book .\"}"));
        });
    }

    private LoreTable books() {
        return db.table(BOOKS);
    }

    void assertTitlesMatch(String expected) {
        assertTitlesMatch(expected, EVERYTHING);
    }

    void assertTitlesMatch(String expected, Criteria criteria) {
        assertEquals(expected, books().select(criteria).map(n -> n.get("title").json()).collect(joining(", ")));
        books().pack();
        assertEquals(expected, books().select(criteria).map(n -> n.get("title").json()).collect(joining(", ")));
    }

}
