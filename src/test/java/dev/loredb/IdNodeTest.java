package dev.loredb;

import dev.loredb.ValueNode.IdNode;
import dev.loredb.auth.LoreUser;
import dev.loredb.index.Ids;
import dev.loredb.io.JsonParser;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IdNodeTest {

    @Test
    public void equality() {
        assertEquals(
            new IdNode(ByteBuffer.allocate(6).putInt(1).putShort((short) 2)),
            new IdNode(ByteBuffer.allocate(6).putInt(1).putShort((short) 2))
        );
    }

    @Test
    public void compressionAndGeneration() {
        assertFormatting("Awt5YBaiAQ", 1627117780642L, 1);
        assertFormatting("Awt5YD6T", 1627117790867L, 0);
    }

    @Test
    public void testGenerationIsUnique() {
        Set<IdNode> idNodes = new HashSet<>();
        Optional<IdNode> same = Stream.generate(LoreUser.ROOT::nextId).parallel().limit(10_000)
                .filter(id -> !idNodes.add(id))
                .findFirst();
        assertTrue("Non-unique for " + same.map(IdNode::toString).orElse("") + " in " + idNodes, same.isEmpty());
    }

    private void assertFormatting(String expected, long timestamp, int userId) {
        IdNode idNode = new IdNode(Ids.createId(timestamp, userId));
        assertEquals(expected, idNode.toString());
        ObjectNode objectNode = (ObjectNode) new JsonParser().parse(String.format("{'_id':'%s'}", expected));
        assertEquals(idNode, objectNode.get("_id"));
    }


}
