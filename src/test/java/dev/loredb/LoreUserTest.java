package dev.loredb;

import dev.loredb.auth.LoreUser;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;

public class LoreUserTest {

    @Test
    public void nextIdUniqueness() {
        var user = new LoreUser(1);
        var previous = JsonNode.NULL;
        for (int i=0; i < 1000; i++) {
            var n = user.nextId();
            assertNotEquals(previous, n);
            previous = n;
        }
    }
}
