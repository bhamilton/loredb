package dev.loredb.criteria;

import dev.loredb.ObjectNode;
import lombok.Data;

import java.util.Collection;
import java.util.function.Predicate;

import static dev.loredb.util.Iterables.newArrayList;
import static java.util.stream.Collectors.joining;

public interface Criteria extends Predicate<ObjectNode> {

    Criteria NOTHING = new Criteria() {
        @Override
        public boolean intersects(Criteria other) {
            return false;
        }
        @Override
        public boolean contains(Criteria other) {
            return false;
        }
        @Override
        public boolean test(ObjectNode objectNode) {
            return false;
        }
    };
    Criteria EVERYTHING = new Criteria() {
        @Override
        public boolean intersects(Criteria other) {
            return true;
        }
        @Override
        public boolean contains(Criteria other) {
            return true;
        }
        @Override
        public boolean test(ObjectNode entries) {
            return true;
        }
    };

    /**
     * Returns true if there is any chance this could intersect with the given criteria.
     */
    boolean intersects(Criteria other);

    /**
     * Returns true only if there is a guaranteed encapsulation of the given criteria.
     */
    boolean contains(Criteria other);

    default Criteria or(Criteria or) {
        if (contains(or))
            return this;
        else if (or.contains(this))
            return or;
        return new Or(this, or);
    }

    default Criteria and(Criteria and) {
        if (contains(and))
            return this;
        else if (and.contains(this))
            return and;
        return new And(this, and);
    }

    @Data
    class And implements Connective {
        private final Collection<Criteria> clauses;

        public And(Criteria left, Criteria right) {
            this(newArrayList(left, right));
        }
        public And(Collection<Criteria> clauses) {
            this.clauses = clauses;
        }

        @Override
        public boolean intersects(Criteria other) {
            return clauses.stream().allMatch(c -> c.intersects(other));
        }

        @Override
        public boolean contains(Criteria other) {
            return clauses.stream().allMatch(c -> c.contains(other));
        }

        @Override
        public boolean test(ObjectNode e) {
            return clauses.stream().allMatch(c -> c.test(e));
        }

        @Override
        public Criteria and(Criteria and) {
            clauses.add(and);
            return this;
        }

        @Override
        public String toString() {
            return clauses.stream()
                    .map(clause -> clause instanceof Connective ? "(" + clause + ")" : clause.toString())
                    .collect(joining(" & "));
        }

    }

    @Data
    class Or implements Connective {
        private final Collection<Criteria> clauses;

        public Or(Collection<Criteria> clauses) {
            this.clauses = clauses;
        }
        public Or(Criteria left, Criteria right) {
            this(newArrayList(left, right));
        }

        @Override
        public boolean intersects(Criteria other) {
            return clauses.stream().anyMatch(c -> c.intersects(other));
        }

        @Override
        public boolean contains(Criteria other) {
            return clauses.stream().anyMatch(c -> c.contains(other));
        }

        @Override
        public boolean test(ObjectNode e) {
            return clauses.stream().anyMatch(c -> c.test(e));
        }

        @Override
        public Criteria or(Criteria or) {
            clauses.add(or);
            return this;
        }

        @Override
        public String toString() {
            return clauses.stream()
                    .map(clause -> clause instanceof Connective ? "(" + clause + ")" : clause.toString())
                    .collect(joining(" | "));
        }

    }

}
