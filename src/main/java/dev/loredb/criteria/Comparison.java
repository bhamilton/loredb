package dev.loredb.criteria;

import dev.loredb.ArrayNode;
import dev.loredb.JsonNode;
import dev.loredb.ObjectNode;
import dev.loredb.ValueNode;
import dev.loredb.ValueNode.StringNode;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public interface Comparison extends Criteria {

    String key();

    CompareOperator operator();

    JsonNode value();

    static Equals eq(String key, Object value) {
        return new Equals(key, JsonNode.of(value));
    }

    static LessThan lessThan(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ValueNode<?>)
            return new LessThan(key, (ValueNode<?>) node);
        else
            throw new IllegalArgumentException();
    }

    static LessThanEquals lessThanEq(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ValueNode<?>)
            return new LessThanEquals(key, (ValueNode<?>) node);
        else
            throw new IllegalArgumentException();
    }

    static GreaterThan greaterThan(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ValueNode<?>)
            return new GreaterThan(key, (ValueNode<?>) node);
        else
            throw new IllegalArgumentException();
    }

    static GreaterThanEquals greaterThanEq(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ValueNode<?>)
            return new GreaterThanEquals(key, (ValueNode<?>) node);
        else
            throw new IllegalArgumentException();
    }

    static RegexMatch matches(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof StringNode)
            return new RegexMatch(key, (StringNode) node);
        else
            throw new IllegalArgumentException();
    }

    static Criteria inBetween(String key, Object lowerBound, Object upperBoundExclusive) {
        return greaterThanEq(key, lowerBound).and(lessThan(key, upperBoundExclusive));
    }

    static InArray in(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ArrayNode)
            return new InArray(key, (ArrayNode) node);
        else
            throw new IllegalArgumentException();
    }

    static NotInArray notIn(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ArrayNode)
            return new NotInArray(key, (ArrayNode) node);
        else
            throw new IllegalArgumentException();
    }

    static Contains contains(String key, Object value) {
        return new Contains(key, JsonNode.of(value));
    }

    static SubsetOf subsetOf(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ArrayNode)
            return new SubsetOf(key, (ArrayNode) node);
        else
            throw new IllegalArgumentException();
    }

    static AnyOf anyOf(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ArrayNode)
            return new AnyOf(key, (ArrayNode) node);
        else
            throw new IllegalArgumentException();
    }

    static NoneOf noneOf(String key, Object value) {
        JsonNode node = JsonNode.of(value);
        if (node instanceof ArrayNode)
            return new NoneOf(key, (ArrayNode) node);
        else
            throw new IllegalArgumentException();
    }

    abstract class ComparisonCriteria<N extends JsonNode> implements Comparison {

        final String key;
        final CompareOperator operator;
        final N value;

        ComparisonCriteria(String key, CompareOperator operator, N value) {
            this.key = key;
            this.operator = operator;
            this.value = value;
        }

        @Override
        public String key() {
            return key;
        }

        @Override
        public CompareOperator operator() {
            return operator;
        }

        @Override
        public N value() {
            return value;
        }

        @Override
        public boolean intersects(Criteria other) {
            return true;
        }

        @Override
        public boolean contains(Criteria other) {
            return false;
        }

        @Override
        public boolean test(ObjectNode n) {
            return testValue(n.get(key));
        }

        abstract boolean testValue(JsonNode n);

        @Override
        public String toString() {
            return key + ' ' + operator.stringValue() + ' ' + value.json();
        }

    }

    class Equals extends ComparisonCriteria<JsonNode> {

        public Equals(String key, JsonNode value) {
            super(key, CompareOperator.EQUALS, value);
        }

        @Override
        public boolean intersects(Criteria other) {
            if (other instanceof Equals) {
                Equals e = (Equals) other;
                return e.key.equals(key) && Objects.equals(e.value, value);
            }
            // TODO ranges
            return false;
        }

        @Override
        public boolean contains(Criteria other) {
            if (other instanceof Equals) {
                Equals e = (Equals) other;
                return e.key.equals(key) && Objects.equals(e.value, value);
            }
            return false;
        }

        @Override
        public boolean testValue(JsonNode n) {
            return Objects.equals(value, n);
        }

    }

    // TODO
    class NotEquals extends ComparisonCriteria<JsonNode> {

        public NotEquals(String key, JsonNode value) {
            super(key, CompareOperator.NOT_EQUALS, value);
        }

        @Override
        public boolean intersects(Criteria other) {
            if (other instanceof NotEquals) {
                NotEquals e = (NotEquals) other;
                return e.key.equals(key) && Objects.equals(e.value, value);
            }
            // TODO ranges
            return false;
        }

        @Override
        public boolean contains(Criteria other) {
            if (other instanceof NotEquals) {
                NotEquals e = (NotEquals) other;
                return e.key.equals(key) && Objects.equals(e.value, value);
            }
            return false;
        }

        @Override
        public boolean testValue(JsonNode n) {
            return !Objects.equals(value, n);
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    class LessThan extends ComparisonCriteria<ValueNode> {

        public LessThan(String key, ValueNode value) {
            super(key, CompareOperator.LESS_THAN, value);
        }

        @Override
        public boolean intersects(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> comparison = (ComparisonCriteria<?>) other;
                if (other instanceof Equals || other instanceof GreaterThan || other instanceof GreaterThanEquals)
                    return comparison.key.equals(key) && value.compareTo((ValueNode) comparison.value) >= 0;
            }
            return true;
        }

        @Override
        public boolean contains(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> comparison = (ComparisonCriteria<?>) other;
                if (other instanceof Equals || other instanceof LessThan || other instanceof LessThanEquals)
                    return comparison.key.equals(key) && value.compareTo((ValueNode) comparison.value) > 0;
            }
            return false;
        }

        @Override
        public boolean testValue(JsonNode n) {
            return value.compareTo((ValueNode) n) > 0;
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    class LessThanEquals extends ComparisonCriteria<ValueNode> {

        public LessThanEquals(String key, ValueNode value) {
            super(key, CompareOperator.LESS_THAN_EQ, value);
        }

        @Override
        public boolean intersects(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> comparison = (ComparisonCriteria<?>) other;
                if (other instanceof Equals || other instanceof GreaterThan || other instanceof GreaterThanEquals)
                    return comparison.key.equals(key) && value.compareTo((ValueNode) comparison.value) > 0;
            }
            return true;
        }

        @Override
        public boolean contains(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> comparison = (ComparisonCriteria<?>) other;
                if (other instanceof Equals || other instanceof LessThan || other instanceof LessThanEquals)
                    return comparison.key.equals(key) && value.compareTo((ValueNode) comparison.value) >= 0;
            }
            return false;
        }

        @Override
        public boolean testValue(JsonNode n) {
            return value.compareTo((ValueNode) n) >= 0;
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    class GreaterThan extends ComparisonCriteria<ValueNode> {

        public GreaterThan(String key, ValueNode value) {
            super(key, CompareOperator.GREATER_THAN, value);
        }

        @Override
        public boolean intersects(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> comparison = (ComparisonCriteria<?>) other;
                if (other instanceof Equals || other instanceof LessThan || other instanceof LessThanEquals)
                    return comparison.key.equals(key) && value.compareTo((ValueNode) comparison.value) >= 0;
            }
            return true;
        }

        @Override
        public boolean contains(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> cmp = (ComparisonCriteria<?>) other;
                if (other instanceof Equals || other instanceof GreaterThan || other instanceof GreaterThanEquals)
                    return cmp.key.equals(key) && value.compareTo((ValueNode) cmp.value) < 0;
            }
            return false;
        }

        @Override
        public boolean testValue(JsonNode n) {
            return value.compareTo((ValueNode) n) < 0;
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    class GreaterThanEquals extends ComparisonCriteria<ValueNode> {

        public GreaterThanEquals(String key, ValueNode value) {
            super(key, CompareOperator.GREATER_THAN_EQ, value);
        }

        @Override
        public boolean intersects(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> cmp = (ComparisonCriteria<?>) other;
                if (cmp.key.equals(key) && (cmp instanceof Equals || cmp instanceof LessThan || cmp instanceof LessThanEquals))
                    return value.compareTo((ValueNode) cmp.value) > 0;
            }
            return true;
        }

        @Override
        public boolean contains(Criteria other) {
            if (other instanceof ComparisonCriteria<?>) {
                ComparisonCriteria<?> cmp = (ComparisonCriteria<?>) other;
                if (cmp instanceof Equals || cmp instanceof GreaterThan || cmp instanceof GreaterThanEquals)
                    return cmp.key.equals(key) && value.compareTo((ValueNode) cmp.value) <= 0;
            }
            return false;
        }

        @Override
        public boolean testValue(JsonNode n) {
            return value.compareTo((ValueNode) n) <= 0;
        }

    }

    class RegexMatch extends ComparisonCriteria<StringNode> {

        final Predicate<String> pattern;
        
        public RegexMatch(String key, StringNode snode) {
            super(key, CompareOperator.REGEX_MATCH, snode);
            pattern = Pattern.compile(snode.getValue(), Pattern.CASE_INSENSITIVE).asMatchPredicate();
        }

        @Override
        public boolean testValue(JsonNode node) {
            return node instanceof StringNode
                    && pattern.test(((StringNode) node).getValue());
        }

    }

    class InArray extends ComparisonCriteria<ArrayNode> {

        public InArray(String key, ArrayNode value) {
            super(key, CompareOperator.IN, value);
        }

        @Override
        public boolean testValue(JsonNode node) {
            return value.children().anyMatch(node::equals);
        }

    }

    class NotInArray extends ComparisonCriteria<ArrayNode> {

        public NotInArray(String key, ArrayNode value) {
            super(key, CompareOperator.NOT_IN, value);
        }

        @Override
        public boolean testValue(JsonNode node) {
            return value.children().noneMatch(node::equals);
        }

    }

    class Contains extends ComparisonCriteria<JsonNode> {

        public Contains(String key, JsonNode value) {
            super(key, CompareOperator.CONTAINS, value);
        }

        @Override
        public boolean testValue(JsonNode node) {
            if (node instanceof ArrayNode) {
                ArrayNode an = (ArrayNode) node;
                return an.children().findAny().isPresent() && an.children().anyMatch(value::equals);
            }
            return false;
        }

    }

    class SubsetOf extends ComparisonCriteria<ArrayNode> {

        public SubsetOf(String key, ArrayNode value) {
            super(key, CompareOperator.SUBSET_OF, value);
        }

        @Override
        public boolean testValue(JsonNode node) {
            if (node instanceof ArrayNode) {
                ArrayNode an = (ArrayNode) node;
                return an.children().findAny().isPresent() && an.children().allMatch(child -> value.children().anyMatch(child::equals));
            }
            return false;
        }

    }

    class AnyOf extends ComparisonCriteria<ArrayNode> {

        public AnyOf(String key, ArrayNode value) {
            super(key, CompareOperator.ANY_OF, value);
        }

        @Override
        public boolean testValue(JsonNode node) {
            return value.children().anyMatch(node::equals);
        }

    }

    class NoneOf extends ComparisonCriteria<ArrayNode> {

        public NoneOf(String key, ArrayNode value) {
            super(key, CompareOperator.NONE_OF, value);
        }

        @Override
        public boolean testValue(JsonNode node) {
            return value.children().noneMatch(node::equals);
        }

    }

}
