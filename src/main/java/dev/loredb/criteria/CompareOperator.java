package dev.loredb.criteria;

import dev.loredb.JsonNode;

import java.util.function.BiFunction;

public enum CompareOperator {
    EQUALS("=", Comparison.Equals::new),
    NOT_EQUALS("!=", Comparison.NotEquals::new),
    LESS_THAN("<", Comparison::lessThan),
    LESS_THAN_EQ("<=", Comparison::lessThanEq),
    GREATER_THAN(">", Comparison::greaterThan),
    GREATER_THAN_EQ(">=", Comparison::greaterThanEq),
    REGEX_MATCH("=~", Comparison::matches),
    IN("in", Comparison::in),
    NOT_IN("nin", Comparison::notIn),
    CONTAINS("contains", Comparison::contains),
    SUBSET_OF("subsetof", Comparison::subsetOf),
    ANY_OF("anyof", Comparison::anyOf),
    NONE_OF("noneof", Comparison::noneOf);

    private final String stringValue;
    private final BiFunction<String, JsonNode, Comparison> comparisonConstructor;

    CompareOperator(String stringValue,
                    BiFunction<String, JsonNode, Comparison> comparisonConstructor) {
        this.stringValue = stringValue;
        this.comparisonConstructor = comparisonConstructor;
    }

    public String stringValue() {
        return stringValue;
    }

}
