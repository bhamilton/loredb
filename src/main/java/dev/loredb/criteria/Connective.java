package dev.loredb.criteria;

import java.util.Collection;

public interface Connective extends Criteria {

    Collection<Criteria> getClauses();

}
