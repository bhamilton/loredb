package dev.loredb;

import dev.loredb.ValueNode.BigIntegerNode;
import dev.loredb.ValueNode.BooleanNode;
import dev.loredb.ValueNode.DoubleNode;
import dev.loredb.ValueNode.IntegerNode;
import dev.loredb.ValueNode.LongNode;
import dev.loredb.ValueNode.NullNode;
import dev.loredb.ValueNode.StringNode;
import dev.loredb.io.JsonOptions;
import dev.loredb.io.JsonParser;
import dev.loredb.io.TextInput;
import dev.loredb.schema.JsonNodeType;
import dev.loredb.util.MapLookup;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public interface JsonNode {

    JsonNode NULL = new NullNode();

    static JsonNode of(Object value) {
        if (value == null)
            return NULL;
        else if (value instanceof JsonNode)
            return (JsonNode) value;
        else if (value instanceof String)
            return new StringNode((String) value);
        else if (value instanceof Integer)
            return new IntegerNode((Integer) value);
        else if (value instanceof Long)
            return new LongNode((Long) value);
        else if (value instanceof Double)
            return new DoubleNode((Double) value);
        else if (value instanceof BigInteger)
            return new BigIntegerNode((BigInteger) value);
        else if (value instanceof Boolean)
            return new BooleanNode((Boolean) value);
        // TODO if type args match
        else if (value instanceof Map) {
            Map<String, JsonNode> map = ((Map<?, ?>) value).entrySet().stream()
                    .collect(toMap(e -> String.valueOf(e.getKey()), e -> JsonNode.of(e.getValue())));
            return new ObjectNode.MapNode(new MapLookup<>(map));
        }
        else if (value instanceof Collection) // TODO if type args match
            return new ArrayNode.CollectionNode(((Collection<?>) value).stream().map(JsonNode::of).collect(toList()));
        else if (value instanceof Enum<?>)
            return new StringNode(((Enum<?>) value).name());
        else
            return new ObjectNode.RecordNode(value);
//        else
//            throw new IllegalArgumentException("Unable to convert to JSON type: " + value);
    }

    static JsonNode fromResource(String resource) {
        InputStream in = JsonNode.class.getResourceAsStream(resource);
        if (in == null)
            throw new IllegalArgumentException("Resource " + resource + " does not exist");
        return new JsonParser().parse(TextInput.wrap(new InputStreamReader(in)));
    }

    default String json() {
        return json(JsonOptions.DEFAULT);
    }

    String json(JsonOptions options);

    /**
     * Convert to the given type.
     * @param type the desired type; must be a record if object node,
     *             collection / array if array node, or primitive / string if value node
     * @return the converted value
     */
    <E> E asA(Class<E> type);

    /**
     * Looser form of asA(Class) for generic type support.
     */
    Object asA(Type type);

    JsonNodeType type();

}
