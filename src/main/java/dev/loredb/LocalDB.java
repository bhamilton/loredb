package dev.loredb;

import dev.loredb.Change.Insert;
import dev.loredb.Change.Update;
import dev.loredb.ObjectNode.RecordNode;
import dev.loredb.ValueNode.IdNode;
import dev.loredb.auth.AuthenticationFailedException;
import dev.loredb.auth.LoreUser;
import dev.loredb.auth.UserAlreadyExistsException;
import dev.loredb.auth.UserDoesNotExistException;
import dev.loredb.criteria.Criteria;
import dev.loredb.schema.JsonSchema;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

import static dev.loredb.criteria.Comparison.eq;

public class LocalDB implements LoreDB {

    private final Map<String, LoreTable> tables;
    private final Function<String, LoreTable> tableFunction;
    private final ThreadLocal<LoreUser> user;
    private final AtomicInteger userIdIncrement;

    public LocalDB(Function<String, ObjectStore> tableFunction) {
        this.tables = new ConcurrentHashMap<>();
        this.tableFunction = k -> new LocalTable(k, tableFunction.apply(k));
        this.user = ThreadLocal.withInitial(() -> LoreUser.ROOT);
        this.userIdIncrement = new AtomicInteger(userTable().selectAll().mapToInt(u -> u.get("number").asA(Integer.class)).max().orElse(0));
        Runtime.getRuntime().addShutdownHook(new Thread(this::pack));
        // TODO should be able to control this
        Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(this::pack, 1000L, 1, TimeUnit.SECONDS);
    }

    @Override
    public LoreTable table(String table) {
        return tables.computeIfAbsent(table, tableFunction);
    }

    @Override
    public LoreUser register(String userName, String password) {
        if (userTable().select(eq("name", userName)).findFirst().isPresent())
            throw new UserAlreadyExistsException(userName);
        // TODO validate password
        var newUser = new LoreUser(userIdIncrement.incrementAndGet(), userName, password);
        userTable().insert(new RecordNode(newUser));
        this.user.set(newUser);
        return newUser;
    }

    @Override
    public LoreDB authenticate(String userName, String challenge) {
        var user = userTable().select(eq("name", userName)).findFirst()
                .map(n -> n.asA(LoreUser.class))
                .orElseThrow(() -> new UserDoesNotExistException(userName));
        if (!user.getPass().equals(challenge)) // TODO plaintext password lol
            throw new AuthenticationFailedException(userName);
        this.user.set(user);
        return this;
    }

    @Override
    public LoreDB authenticate(Integer userId) {
        this.user.set(userForId(userId));
        return this;
    }

    private LoreUser userForId(Integer userId) {
        if (userId == 0)
            return LoreUser.ROOT;
        return userTable().select(eq("number", userId)).findFirst()
                .map(n -> n.asA(LoreUser.class))
                .orElseThrow(() -> new UserDoesNotExistException(userId));
    }

    private void pack() {
        for (var table : tables.values())
            table.pack();
    }

    private LoreTable userTable() {
        return table(LoreUser.TABLE);
    }

    @Override
    public LoreUser getUser() {
        return user.get();
    }

    class LocalTable implements LoreTable {

        private final String name;
        private final ObjectStore base;

        private final Map<IdNode, ChangeListener> listeners;
        private final List<Change> history;

        public LocalTable(String name,
                          ObjectStore base) {
            this(name, base, new ConcurrentSkipListMap<>(), new Vector<>());
        }

        public LocalTable(String name,
                          ObjectStore base,
                          Map<IdNode, ChangeListener> listeners,
                          List<Change> history) {
            this.name = name;
            this.base = base;
            this.listeners = listeners;
            this.history = history;
        }

        @Override
        public String name() {
            return name;
        }

        @Override
        public LoreUser user() {
            return user.get();
        }

        @Override
        public void accept(Change change) {
            validate(change);
            history.add(change);
            listeners.forEach((key, listener) -> {
                if (listener.criteria().intersects(change.criteria()))
                    listener.onChange(change);
            });
        }

        private void validate(Change change) {
            if (change instanceof Insert)
                base.schema().validate(((Insert) change).getNode());
            else if (change instanceof Update)
                base.schema().validate(((Update) change).getChange().apply(ObjectNode.empty()));
        }

        @Override
        public Stream<ObjectNode> select(Criteria criteria) {
            return readWithChange().filter(criteria);
        }

        @Override
        public String subscribe(ChangeListener listener) {
            IdNode key = user().nextId();
            listeners.put(key, listener);
            return key.toString();
        }

        @Override
        public void unsubscribe(String key) {
            try {
                listeners.remove(new IdNode(key));
            }
            catch (RuntimeException ignored) {}
        }

        @Override
        public void changeSchema(JsonSchema schema) {
            base.schema(schema);
        }

        @Override
        public void pack() {
            if (history.isEmpty())
                return;
            List<Change> currentHistory = new ArrayList<>(history);
            history.clear();
            base.save(readWithChange(currentHistory));
        }

        private Stream<ObjectNode> readWithChange() {
            return readWithChange(history);
        }

        private Stream<ObjectNode> readWithChange(List<Change> history) {
            Stream<ObjectNode> results = base.stream();
            for (Change change : history)
                results = change.apply(results);
            return results;
        }

    }

}
