package dev.loredb;

import dev.loredb.io.JsonOptions;
import dev.loredb.schema.JsonNodeType;
import dev.loredb.util.Reflection;
import lombok.Data;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public interface ArrayNode extends JsonNode {

    Stream<? extends JsonNode> children();
    
    default Stream<ObjectNode> records() {
        return children().filter(ObjectNode.class::isInstance).map(ObjectNode.class::cast);
    }

    default Optional<? extends JsonNode> first() {
        return children().findFirst();
    }

    default void forEach(Consumer<JsonNode> consumer) {
        children().forEach(consumer);
    }

    default JsonNodeType type() {
        return JsonNodeType.array;
    }

    @Override
    default <E> E asA(Class<E> type) {
        if (type.isArray()) {
            return type.cast(children()
                    .map(n -> n.asA(type.getComponentType()))
                    .toArray(len -> (Object[]) Array.newInstance(type.getComponentType(), len)));
        }
        throw new IllegalArgumentException("Expected array or collection for array node conversion but was " + type.getName());
    }

    @Override
    default Object asA(Type type) {
        if (type instanceof Class<?>)
            return asA((Class<?>) type);
        if (type instanceof ParameterizedType
                && Collection.class.isAssignableFrom(((Class<?>) ((ParameterizedType) type).getRawType()))) {
            Type typeBound = ((ParameterizedType) type).getActualTypeArguments()[0];
            Collector<Object, ?, ? extends Collection<Object>> collector = Reflection.collectorForCollectionType(((ParameterizedType) type));
            return children()
                    .map(n -> n.asA(typeBound))
                    .collect(collector);
        }
        throw new IllegalArgumentException("Expected array or collection for ArrayNode conversion but was " + type.getTypeName());
    }

    default String json(JsonOptions options) {
        return '[' + children().map(n -> n.json(options)).collect(joining(",")) + ']';
    }

    @Data
    class CollectionNode implements ArrayNode {
        private final Collection<JsonNode> value;
        @Override
        public Stream<JsonNode> children() {
            return value.stream();
        }

        @Override
        public String json() {
            return '[' + children().map(JsonNode::json).collect(joining(",")) + ']';
        }
    }

}
