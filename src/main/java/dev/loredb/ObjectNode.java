package dev.loredb;

import dev.loredb.ValueNode.IdNode;
import dev.loredb.ValueNode.IntegerNode;
import dev.loredb.ValueNode.TimeNode;
import dev.loredb.io.JsonOptions;
import dev.loredb.schema.JsonNodeType;
import dev.loredb.util.CacheLookup;
import dev.loredb.util.Iterables;
import dev.loredb.util.Lookup;
import dev.loredb.util.MergedLookup;
import dev.loredb.util.Streams;
import lombok.Data;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import static dev.loredb.util.Reflection.nonStaticFields;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

public interface ObjectNode extends JsonNode, Lookup<String, JsonNode> {

    static ObjectNode empty() {
        Lookup<String, JsonNode> lookup = new CacheLookup<String, JsonNode>(k -> NULL);
        return new MapNode(lookup);
    }

    default JsonNode get(String key) {
        for (var entry : this)
            if (entry.getKey().equals(key))
                return entry.getValue();
        return NULL;
    }

    default void entries(BiConsumer<String, JsonNode> consumer) {
        entries().forEach(e -> consumer.accept(e.getKey(), e.getValue()));
    }

    default Stream<Entry<String, JsonNode>> entries() {
        return Streams.toStream(iterator());
    }

    default Stream<Entry<String, JsonNode>> entriesDetailed() {
        return entries().flatMap(e -> {
            if (e.getKey().equals("_id") && e.getValue() instanceof ValueNode.IdNode) {
                var idNode = (IdNode) e.getValue();
                return Stream.of(
                        e,
                        new SimpleImmutableEntry<>("_creator", new IntegerNode(idNode.userNumber())),
                        new SimpleImmutableEntry<>("_created", new TimeNode(idNode.instant()))
                );
            }
            return Stream.of(e);
        });
    }

    default JsonNodeType type() {
        return JsonNodeType.object;
    }

    @Override
    default ObjectNode assign(Lookup<String, JsonNode> other) {
        return new MapNode(new MergedLookup<>(this, other));
    }

    @Override
    default ObjectNode assign(String key, JsonNode value) {
        return assign(Lookup.of(key, value));
    }

    default ObjectNode assign(String key, Object value) {
        return assign(Lookup.of(key, JsonNode.of(value)));
    }

    default String json(JsonOptions options) {
        Stream<Entry<String, JsonNode>> entries = options.isDetailed() ? entriesDetailed() : entries();
        return '{' + entries.map(e -> '"' + e.getKey() + "\":" + e.getValue().json(options)).collect(joining(",")) + '}';
    }

    default <T> T asA(Class<T> type) {
        // TODO assumes record
        try {
            var constructors = type.getDeclaredConstructors();
            Arrays.sort(constructors, comparingInt(Constructor::getParameterCount));
            var largestConstructor = constructors[constructors.length - 1];
            // TODO handle empty constructor, etc.
            Field[] declaredFields = nonStaticFields(type)
                    .toArray(Field[]::new);
            if (largestConstructor.getParameterCount() != declaredFields.length)
                throw new IllegalArgumentException("Ambiguous field/constructor alignment. Expected equal number of fields to constructor arguments.");
            var args = Arrays.stream(declaredFields)
                    .map(field -> get(field.getName()).asA(field.getGenericType()))
                    .toArray();
            var record = largestConstructor.newInstance(args);
            return type.cast(record);
        } catch (ReflectiveOperationException e) {
            throw new IllegalArgumentException("Failed to create instance of " + type, e);
        }
    }

    @Override
    default Object asA(Type type) {
        if (type instanceof Class<?>)
            return asA((Class<?>) type);
        if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            if (pt.getRawType().equals(Optional.class))
                return Optional.of(asA(pt.getActualTypeArguments()[0]));
            else if (pt.getRawType().equals(Map.class) && pt.getActualTypeArguments()[0] == String.class)
                return entries().collect(toMap(Entry::getKey, e -> e.getValue().asA(pt.getActualTypeArguments()[1])));
        }
        throw new IllegalArgumentException("Type " + type + " not supported for object node conversion");
    }

    @Data
    class RecordNode implements ObjectNode {
        private final Object record;
        @Override
        public Iterator<Entry<String, JsonNode>> iterator() {
            return nonStaticFields(record.getClass())
                    .map(this::asEntry)
                    .filter(e -> !NULL.equals(e.getValue()))
                    .iterator();
        }
        private Entry<String, JsonNode> asEntry(Field field) {
            try {
                Method getter = getGetter(field);
                JsonNode value = JsonNode.of(getter.invoke(record));
                return new SimpleImmutableEntry<>(field.getName(), value);
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public JsonNode get(String key) {
            try {
                Field field = record.getClass().getDeclaredField(key);
                Method getter = getGetter(field);
                return JsonNode.of(getter.invoke(record));
            } catch (NoSuchMethodException e) {
                return NULL;
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
        }

        private Method getGetter(Field field) throws ReflectiveOperationException {
            String getterName = field.getType().equals(Boolean.class) || field.getType().equals(Boolean.TYPE)
                    ? "is" + capitalizeFirst(field.getName())
                    : "get" + capitalizeFirst(field.getName());
            return record.getClass().getMethod(getterName);
        }

        private String capitalizeFirst(String str) {
            return Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
    }

    @Data
    class MapNode implements ObjectNode {
        private final Lookup<String, JsonNode> value;
        @Override
        public Iterator<Entry<String, JsonNode>> iterator() {
            return Iterables.filter(value.iterator(), e -> !NULL.equals(e.getValue()));
        }
        @Override
        public JsonNode get(String key) {
            return value.getOrDefault(key, NULL);
        }
    }

}
