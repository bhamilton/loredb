package dev.loredb;

import java.security.SecureRandom;

// TODO I don't like this
public final class IdGenerator {

    private static final SecureRandom RANDOM = new SecureRandom();

    public static long nextId() {
        return Math.abs(RANDOM.nextLong());
    }

}
