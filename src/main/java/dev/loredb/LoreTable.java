package dev.loredb;

import dev.loredb.Change.Delete;
import dev.loredb.Change.Insert;
import dev.loredb.Change.Update;
import dev.loredb.auth.LoreUser;
import dev.loredb.criteria.Criteria;
import dev.loredb.io.JsonParser;
import dev.loredb.schema.JsonSchema;

import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public interface LoreTable extends Consumer<Change> {

    /**
     * Get the name of the table.
     */
    String name();

    /**
     * Get the current user.
     */
    LoreUser user();

    /**
     * Insert new record as JSON string.
     */
    default void insert(String json) {
        JsonNode node = new JsonParser().parse(json);
        if (node instanceof ObjectNode)
            insert((ObjectNode) node);
        else if (node instanceof ArrayNode)
            insert((ArrayNode) node);
        else
            throw new IllegalArgumentException("Object or array nodes allowed only");
    }

    /**
     * Insert multiple new records as array.
     */
    default void insert(ArrayNode node) {
        node.children().forEach(child -> {
            if (child instanceof ObjectNode)
                insert((ObjectNode) child);
            else
                throw new IllegalArgumentException();
        });
    }

    /**
     * Insert new record.
     */
    default void insert(ObjectNode node) {
        accept(new Insert(user(), node));
    }

    /**
     * Update multiple records matching given criteria.
     */
    default void update(Criteria criteria, UnaryOperator<ObjectNode> change) {
        accept(new Update(user(), criteria, change));
    }

    /**
     * Delete all records matching the given criteria.
     */
    default void delete(Criteria criteria) {
        accept(new Delete(user(), criteria));
    }

    /**
     * Query records matching the given criteria.
     */
    default Stream<ObjectNode> selectAll() {
        return select(Criteria.EVERYTHING);
    }

    /**
     * Query records matching the given criteria.
     */
    Stream<ObjectNode> select(Criteria criteria);

    /**
     * Listen for changes on the given collection.
     */
    default String subscribe(Consumer<Change> listener) {
        return subscribe(Criteria.EVERYTHING, listener);
    }

    /**
     * Listen for changes on the given collection.
     */
    default String subscribe(Criteria criteria, Consumer<Change> listener) {
        return subscribe(new ChangeListener() {
            @Override
            public Criteria criteria() {
                return criteria;
            }
            @Override
            public void onChange(Change change) {
                listener.accept(change);
            }
        });
    }

    /**
     * Listen for changes on the given collection.
     */
    String subscribe(ChangeListener listener);

    /**
     * Remove the listener for the given key.
     */
    void unsubscribe(String key);

    /**
     * Change the schema for a given collection.
     */
    void changeSchema(JsonSchema schema);

    /**
     * Manually store all changes to tables.
     */
    void pack();

}
