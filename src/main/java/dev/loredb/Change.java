package dev.loredb;

import dev.loredb.auth.LoreUser;
import dev.loredb.criteria.Criteria;
import dev.loredb.index.Ids;
import lombok.Data;

import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * Ch-ch-ch-ch-chayanges!
 */
// TODO boolean optimisation
public interface Change extends UnaryOperator<Stream<ObjectNode>> {

    LoreUser getUser();

    default Criteria criteria() {
        return Criteria.NOTHING;
    }

    @Data
    class Insert implements Change {
        private final LoreUser user;
        private final ObjectNode node;

        public Insert(LoreUser user, ObjectNode node) {
            this.user = user;
            this.node = node.assign(Ids.ATTR, user.nextId());
        }

        @Override
        public Stream<ObjectNode> apply(Stream<ObjectNode> s) {
            return Stream.concat(Stream.of(node), s);
        }
    }

    @Data
    class Update implements Change {
        private final LoreUser user;
        private final Criteria criteria;
        private final UnaryOperator<ObjectNode> change;
        @Override
        public Stream<ObjectNode> apply(Stream<ObjectNode> s) {
            return s.map(n -> user.canModify(n) && criteria.test(n) ? change.apply(n) : n);
        }
    }

    @Data
    class Delete implements Change {
        private final LoreUser user;
        private final Criteria criteria;
        @Override
        public Stream<ObjectNode> apply(Stream<ObjectNode> s) {
            return s.filter(n -> !(user.canModify(n) && criteria.test(n)));
        }
    }

}
