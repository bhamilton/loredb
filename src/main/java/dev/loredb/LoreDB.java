package dev.loredb;

import dev.loredb.auth.LoreUser;
import dev.loredb.io.JsonObjectStore;

import java.nio.file.Path;
import java.nio.file.Paths;

public interface LoreDB {

    /**
     * Create a new in-memory database.
     */
    static LoreDB inMemory() {
        return new LocalDB(table -> JsonObjectStore.inMemory());
    }

    /**
     * Create a new database from root folder.
     */
    static LoreDB fromPath(String path) {
        return fromPath(Paths.get(path));
    }

    /**
     * Create a new database from root folder.
     */
    static LoreDB fromPath(Path path) {
        return new LocalDB(table -> JsonObjectStore.fromPath(path.resolve(table)));
    }

    /**
     * Get the table for the given name.
     */
    LoreTable table(String table);

    /**
     * Create a new user with the given username and password. Implicitly authenticates new user.
     */
    LoreUser register(String userName, String password);

    /**
     * Configure the context to use the given user credentials.
     */
    LoreDB authenticate(String userName, String password);

    /**
     * Configure the context user ID.
     */
    LoreDB authenticate(Integer userId);

    LoreUser getUser();

}
