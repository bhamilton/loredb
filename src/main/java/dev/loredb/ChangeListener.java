package dev.loredb;

import dev.loredb.criteria.Criteria;

public interface ChangeListener {
    Criteria criteria();
    void onChange(Change change);
}
