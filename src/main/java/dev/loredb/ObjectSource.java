package dev.loredb;

import dev.loredb.ObjectNode;
import dev.loredb.criteria.Criteria;

import java.util.stream.Stream;

import static dev.loredb.criteria.Criteria.EVERYTHING;

public interface ObjectSource {

    default Stream<ObjectNode> all() {
        return find(EVERYTHING);
    }

    Stream<ObjectNode> find(Criteria criteria);

}
