package dev.loredb.schema;

import dev.loredb.ArrayNode;
import dev.loredb.JsonNode;
import dev.loredb.ObjectNode;
import lombok.Data;

import java.util.Map;

import static dev.loredb.schema.JsonProperties.isSpecial;

// TODO required
@Data
public class JsonSchema implements JsonSchemaValidator {

    private final JsonNodeType type;
    private final Map<String, JsonSchema> properties;     // types of properties for object node
    private final JsonSchema items;                       // type of child element for arrays

    public static JsonSchema empty() {
        return new JsonSchema(null, null, null);
    }

    // Recursively validates schema of chain of nodes from root to leaf.
    @Override
    public void validate(JsonNode node) {
        if (type == null)
            return;
        if (!node.type().equals(type))
            throw new JsonSchemaException(String.format("Type mismatch, expected %s but was %s: %s", type, node.type(), node));
        switch (type) {
            case array: validateArray((ArrayNode) node); break;
            case object: validateObject((ObjectNode) node); break;
            // more?
        }
    }

    private void validateArray(ArrayNode node) {
        // element schema specified via "items" property
        if (items != null)
            node.children().forEach(items::validate);
    }

    private void validateObject(ObjectNode node) {
        // no properties, no validation needed.
        if (properties == null)
            return;
        // validate all properties if last node in chain
        node.entries(this::validateProperty);
    }

    private void validateProperty(String key, JsonNode value) {
        if (isSpecial(key)) // TODO should validate special key
            return;
        if (!properties.containsKey(key))
            throw new JsonSchemaException(String.format("Expected one of %s as property but was %s", properties.keySet(), key));
        properties.get(key).validate(value);
    }

}
