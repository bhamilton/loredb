package dev.loredb.schema;

public enum JsonNodeType {
    array,
    bool,
    nil,
    number,
    object,
    string,
    time,
    id
}
