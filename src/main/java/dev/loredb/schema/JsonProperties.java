package dev.loredb.schema;

import dev.loredb.index.Ids;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

public final class JsonProperties {

    private static final Set<String> SPECIAL = new HashSet<>(asList(Ids.ATTR));

    public static boolean isSpecial(String key) {
        return SPECIAL.contains(key);
    }

}
