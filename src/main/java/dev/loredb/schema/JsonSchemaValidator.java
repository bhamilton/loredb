package dev.loredb.schema;

import dev.loredb.JsonNode;

public interface JsonSchemaValidator {

    void validate(JsonNode node);

}
