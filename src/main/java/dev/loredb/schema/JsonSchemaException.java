package dev.loredb.schema;

public class JsonSchemaException extends IllegalArgumentException {

    public JsonSchemaException(String s) {
        super(s);
    }

}
