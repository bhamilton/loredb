package dev.loredb.index;

import dev.loredb.ObjectNode;
import dev.loredb.auth.LoreUser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.Instant;
import java.util.Base64;
import java.util.function.UnaryOperator;

public final class Ids {

    public static final String ATTR = "_id";
    public static final int BYTES = Long.BYTES + Integer.BYTES;

    private static final long YEAR_2020 = Instant.parse("2020-01-01T00:00:00Z").toEpochMilli();

    // timestamp is incremented for ids that fall in the same millisecond
    // the assumption is that either it won't occur often or the precision isn't that important
    private static final Object lock = new Object();
    private static volatile long timestamp = System.currentTimeMillis();

    public static UnaryOperator<ObjectNode> assign(LoreUser user) {
        return n -> n.assign(ATTR, user.nextId());
    }

    /**
     * We trim off the excess 0's from the start and end of the bytes.
     * TODO there is a more efficient way than using start index...
     */
    public static String writeCompressed(ByteBuffer buffer) {
        int start = 0, end = buffer.limit() - 1;
        for (; start < buffer.limit(); start++)
            if (buffer.get(start) != 0)
                break;
        for (; end > start; end--)
            if (buffer.get(end) != 0)
                break;
        int length = end - start + 1;
        byte[] bytes = new byte[length + 1];
        bytes[0] = (byte) start;
        buffer.position(start).get(bytes, 1, length);
        // buffer.get(start, bytes, 1, length);
        return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
    }

    /**
     * Read compressed string to byte buffer of correct ID length (12 bytes)
     */
    public static ByteBuffer readCompressed(String base64String) {
        byte[] bytes = Base64.getUrlDecoder().decode(base64String);
        int start = bytes[0];
        return ByteBuffer.allocate(Ids.BYTES).position(start)
                .put(bytes, 1, bytes.length - 1)
                .position(0);
    }

    public static ByteBuffer nextIdForUser(int userId) {
        synchronized (lock) {
            return createId(timestamp = Math.max(System.currentTimeMillis(), timestamp + 1), userId);
        }
    }

    public static int readUserNumber(ByteBuffer id) {
        return id.asReadOnlyBuffer().position(Long.BYTES).getInt();
    }

    public static long readTimestamp(ByteBuffer id) {
        return id.asReadOnlyBuffer().position(0).getLong() + YEAR_2020;
    }

    // we reverse the endian order part way so zeroes are clustered to the ends for better compression
    public static ByteBuffer createId(long timestamp, int userId) {
        return ByteBuffer.allocate(Ids.BYTES)
                .putLong(timestamp - YEAR_2020)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putInt(userId)
                .position(0);
    }

}
