package dev.loredb.auth;

import dev.loredb.JsonNode;
import dev.loredb.ObjectNode;
import dev.loredb.ValueNode.IdNode;
import dev.loredb.index.Ids;
import lombok.Data;

@Data
public class LoreUser {

    public static final LoreUser ROOT = new LoreUser(0);
    public static final String TABLE = "_users";

    private final int number;
    private final String name;
    private final String pass;

    public LoreUser(int number, String name, String pass) {
        this.number = number;
        this.name = name;
        this.pass = pass;
    }

    public LoreUser(int number) {
        this(number, "root", "");
    }

    public IdNode nextId() {
        return new IdNode(Ids.nextIdForUser(number));
    }

    public boolean canModify(ObjectNode n) {
        // root access
        if (number == 0)
            return true;
        // ids are equivalent
        JsonNode id = n.get("id");
        if (id instanceof IdNode)
            return ((IdNode) id).userNumber() == number;
        // normal users cannot modify records without ids
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoreUser)) return false;
        return number == ((LoreUser) o).number;
    }

    @Override
    public int hashCode() {
        return number;
    }

}
