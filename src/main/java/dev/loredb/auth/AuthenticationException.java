package dev.loredb.auth;

public class AuthenticationException extends IllegalArgumentException {
    public AuthenticationException(String s) {
        super(s);
    }
}
