package dev.loredb.auth;

public class AuthenticationFailedException extends AuthenticationException {
    public AuthenticationFailedException(String userName) {
        super("Invalid credentials submitted by " + userName);
    }
}
