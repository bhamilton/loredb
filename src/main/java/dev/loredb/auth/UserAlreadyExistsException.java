package dev.loredb.auth;

public class UserAlreadyExistsException extends AuthenticationException {
    public UserAlreadyExistsException(String userName) {
        super(String.format("User %s already exists!", userName));
    }
}
