package dev.loredb.auth;

public class UserDoesNotExistException extends IllegalArgumentException {

    public UserDoesNotExistException(Object username) {
        super(String.format("User %s does not exist!", username));
    }

}
