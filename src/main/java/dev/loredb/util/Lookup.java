package dev.loredb.util;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;

import static java.util.stream.Collectors.toMap;

public interface Lookup<K, V> extends Iterable<Entry<K, V>> {

    static <K, V> Lookup<K, V> of(K key, V value) {
        return new Lookup<>() {
            @Override
            public V get(K k) {
                return key.equals(k) ? value : null;
            }
            @Override
            public Iterator<Entry<K, V>> iterator() {
                return Collections.<Entry<K, V>>singleton(new SimpleImmutableEntry<>(key, value)).iterator();
            }
        };
    }

    V get(K key);

    default V getOrDefault(K key, V defaultValue) {
        V value = get(key);
        return value == null ? defaultValue : value;
    }

    default Iterator<Entry<K, V>> iterator() {
        return Collections.emptyIterator();
    }

    default Lookup<K, V> assign(Lookup<K, V> other) {
        return new MergedLookup<>(this, other);
    }

    default Lookup<K, V> assign(K key, V value) {
        return assign(Lookup.of(key, value));
    }

}
