package dev.loredb.util;

public interface LazyLoading {
    void load();
}
