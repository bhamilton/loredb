package dev.loredb.util;

public interface LazyLoadingIterable<E> extends Iterable<E>, LazyLoading {
}
