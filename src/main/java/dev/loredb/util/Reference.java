package dev.loredb.util;

public interface Reference<T> {

    void set(T ref);

    T get();

}
