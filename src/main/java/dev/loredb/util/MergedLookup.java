package dev.loredb.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import static dev.loredb.util.Iterables.*;

public class MergedLookup<K, V> implements Lookup<K, V> {

    private final Lookup<K, V> base;
    private final Lookup<K, V> extension;

    public MergedLookup(Lookup<K, V> base,
                        Lookup<K, V> extension) {
        this.extension = extension;
        this.base = base;
    }

    @Override
    public V get(K key) {
        V value = extension.get(key);
        return value == null ? base.get(key) : value;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        Set<K> seen = new HashSet<>();
        return concat(
            onNext(extension.iterator(), e -> seen.add(e.getKey())),
            filter(base.iterator(), e -> !seen.contains(e.getKey()))
        );
    }
}
