package dev.loredb.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static java.util.stream.Collectors.toUnmodifiableSet;

public class Reflection {

    public static Collector<Object, ?, ? extends Collection<Object>> collectorForCollectionType(ParameterizedType pt) {
        switch (pt.getRawType().getTypeName()) {
            case "java.util.List": case "java.util.Collection": return toUnmodifiableList();
            case "java.util.Set": return toUnmodifiableSet();
            default: throw new IllegalArgumentException("Unrecognized collection type " + pt.getTypeName());
        }
    }

    public static Stream<Field> nonStaticFields(Class<?> type) {
        return Arrays.stream(type.getDeclaredFields())
                .filter(f -> !Modifier.isStatic(f.getModifiers()));

    }

}
