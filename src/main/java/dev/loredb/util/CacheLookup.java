package dev.loredb.util;

import java.util.HashMap;
import java.util.function.Function;

public class CacheLookup<K, V> extends MapLookup<K, V> {

    final Function<K, V> populator;

    public CacheLookup(Function<K, V> populator) {
        super(new HashMap<>());
        this.populator = populator;
    }

    @Override
    public V get(K key) {
        return map.computeIfAbsent(key, populator);
    }

}
