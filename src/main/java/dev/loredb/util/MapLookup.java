package dev.loredb.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapLookup<K, V> implements Lookup<K, V> {

    final Map<K, V> map;

    public MapLookup(Map<K, V> map) {
        this.map = map;
    }

    @Override
    public V get(K key) {
        return map.get(key);
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return map.entrySet().iterator();
    }

}
