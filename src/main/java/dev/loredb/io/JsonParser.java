package dev.loredb.io;

import dev.loredb.ArrayNode;
import dev.loredb.JsonNode;
import dev.loredb.ObjectNode;
import dev.loredb.ValueNode.BigIntegerNode;
import dev.loredb.ValueNode.BooleanNode;
import dev.loredb.ValueNode.DoubleNode;
import dev.loredb.ValueNode.IdNode;
import dev.loredb.ValueNode.IntegerNode;
import dev.loredb.ValueNode.LongNode;
import dev.loredb.ValueNode.StringNode;
import dev.loredb.index.Ids;
import dev.loredb.util.Iterables;
import dev.loredb.util.LazyLoading;
import dev.loredb.util.LazyLoadingIterable;
import dev.loredb.util.Streams;

import java.math.BigInteger;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.stream.Stream;

import static dev.loredb.util.Shorthands.then;
import static java.lang.Character.isDigit;
import static java.lang.Character.isLetter;

public class JsonParser implements Parser<JsonNode> {

    private static final int
            MAX_INT_DIGITS = String.valueOf(Integer.MAX_VALUE).length(),
            MAX_LONG_DIGITS = String.valueOf(Long.MAX_VALUE).length() + 1;

    @Override
    public JsonNode parse(TextInput text) {
        try {
            if (!text.hasNext())
                return JsonNode.NULL;
            final int next = text.skipWhitespace().peek();
            switch (next) {
                case '"': case '\'': return stringNode(text, (char) next);
                case '0': case '1': case '2': case '3': case '4': case '5':
                    case '6': case '7': case '8': case '9': case '-': return numberNode(text);
                case 'f': case 't': return booleanNode(text.read(next == 'f' ? 5 : 4));
                case 'n': return nullNode(text);
                case '[': return new LazyLoadArrayNode(text);
                case '{': return new LazyLoadObjectNode(text);
                default:
                    throw new JsonParseException("Expected json node/value, but was character " + ((char) next), text);
            }
        } catch (JsonParseException e) {
            throw e.setText(text);
        } catch (RuntimeException e) {
            throw new JsonParseException(e, text);
        }
    }

    private JsonNode stringNode(TextInput text, char quote) {
        return new StringNode(readStringValue(text, quote).toString());
    }

    private JsonNode parseId(TextInput text) {
        final int next = text.skipWhitespace().peek();
        switch (next) {
            case '"': case '\'': return new IdNode(readStringValue(text, (char) next).toString());
            case 'n': return nullNode(text);
            default: throw new JsonParseException("Expected quote");
        }
    }

    private StringBuilder readStringValue(TextInput text, char quote) {
        text.skip(quote);
        final StringBuilder sb = new StringBuilder();
        while (text.hasNext() && text.peek() != quote) {
            int c = text.read();
            if (c == '\\' && text.hasNext()) {
                c = text.read();
                switch (c) {
                    case '"': case '\'': case '\\': case '/': sb.append((char) c); break;
                    case 'b': sb.deleteCharAt(sb.length() - 1); break;
                    case 'f': sb.append('\f'); break;
                    case 'n': sb.append('\n'); break;
                    case 'r': sb.append('\r'); break;
                    case 't': sb.append('\t'); break;
                    case 'u': sb.appendCodePoint(Integer.parseInt(text.read(4), 16)); break;
                    default: sb.append('\\').append((char) c);
                }
            } else {
                sb.append((char) c);
            }
        }
        text.skip(quote);
        return sb;
    }

    private JsonNode nullNode(TextInput text) {
        text.skipIgnoreCase("null");
        return JsonNode.NULL;
    }

    private JsonNode numberNode(TextInput text) {
        final String stringValue = text.readWhile(this::isNumeric);
        if (stringValue.matches("-?\\d+")) {
            if (stringValue.length() < MAX_INT_DIGITS)
                return new IntegerNode(Integer.parseInt(stringValue));
            else if (stringValue.length() < MAX_LONG_DIGITS)
                return new LongNode(Long.parseLong(stringValue));
            else
                return new BigIntegerNode(new BigInteger(stringValue));
        }
        else if (stringValue.matches("-?\\d+(?:\\.\\d+)?(?:[eE][+-]?\\d+)?"))
            return new DoubleNode(Double.parseDouble(stringValue));
        else
            throw new JsonParseException("Expected number value but was \"" + stringValue + "\".");
    }

    boolean isNumeric(int ch) {
        return isDigit(ch) || ch == 'e' || ch == 'E' || ch == '.' || ch == '-' || ch == '+';
    }

    private JsonNode booleanNode(String stringValue) {
        if (stringValue.equals("true"))
            return new BooleanNode(true);
        else if (stringValue.equals("false"))
            return new BooleanNode(false);
        throw new JsonParseException("Expected boolean value but was \"" + stringValue + "\".");
    }

    private class LazyLoadArrayNode implements ArrayNode, LazyLoading {

        private final LazyLoadingIterable<JsonNode> children;

        public LazyLoadArrayNode(TextInput text) {
            children = Iterables.recalling(new ArrayNodeTextIterator(text));
        }

        @Override
        public Stream<JsonNode> children() {
            return Streams.toStream(children);
        }

        @Override
        public void load() {
            children.load();
        }

        @Override
        public String toString() {
            return json();
        }

    }

    private class LazyLoadObjectNode implements ObjectNode, LazyLoading {

        private final LazyLoadingIterable<Entry<String, JsonNode>> children;

        public LazyLoadObjectNode(TextInput text) {
            this.children = Iterables.recalling(new ObjectNodeTextIterator(text));
        }

        @Override
        public Iterator<Entry<String, JsonNode>> iterator() {
            return children.iterator();
        }

        @Override
        public void load() {
            children.load();
        }

        @Override
        public String toString() {
            return json();
        }

    }

    /**
     * Lazily loads children from text input.
     *
     * Extended for use in object node and array node.
     */
    private static abstract class JsonChildNodeTextIterator<N> implements Iterator<N> {

        protected final TextInput text;
        private final char startChar, endChar;
        private boolean started = false, done = false;
        protected N previous = null;

        JsonChildNodeTextIterator(TextInput text, char startChar, char endChar) {
            this.text = text;
            this.startChar = startChar;
            this.endChar = endChar;
        }

        @Override
        public boolean hasNext() {
            if (done || !text.hasNext())
                return false;
            advanceFromPrevious();
            return !((done = text.skipWhitespace().peek() == endChar) && text.skip(endChar) != null);
        }

        void advanceFromPrevious() {
            if (previous == null) {
                if (!started)
                    text.skip(startChar);
                started = true;
            } else {
                completePrevious();
            }
        }

        abstract void completePrevious();

        void skipComma() {
            final int peek = text.peek();
            if (peek == ',')
                text.skip();
        }

        N setPrevious(N node) {
            return this.previous = node;
        }

    }

    private class ObjectNodeTextIterator extends JsonChildNodeTextIterator<Entry<String, JsonNode>> {

        ObjectNodeTextIterator(TextInput text) {
            super(text, '{', '}');
        }

        @Override
        public Entry<String, JsonNode> next() {
            skipComma();
            final int peek = text.skipWhitespace().peek();
            final String property;
            if (peek == '\'' || peek == '"')
                property = then(text.skip().readUntilWithEscape((char) peek, '\\'), text::skip);
            else if (isLetter(peek))
                property = text.readWhile(Character::isLetterOrDigit);
            else
                throw new JsonParseException("Expected letter or quote but was " + ((char) peek), text);
            text.skipWhitespace().skipIgnoreCase(':').skipWhitespace();
            JsonNode value = property.equals(Ids.ATTR)
                    ? parseId(text)
                    : parse(text);
            return setPrevious(new SimpleImmutableEntry<>(property, value));
        }

        @Override
        void completePrevious() {
            JsonNode previousValue = previous.getValue();
            if (previousValue instanceof LazyLoading)
                ((LazyLoading) previousValue).load();
        }
    }

    private class ArrayNodeTextIterator extends JsonChildNodeTextIterator<JsonNode> {

        ArrayNodeTextIterator(TextInput text) {
            super(text, '[', ']');
        }

        @Override
        public JsonNode next() {
            skipComma();
            text.skipWhitespace();
            return setPrevious(parse(text));
        }

        @Override
        void completePrevious() {
            if (previous instanceof LazyLoading)
                ((LazyLoading) previous).load();
        }
    }
}
