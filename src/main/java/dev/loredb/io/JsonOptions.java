package dev.loredb.io;

import lombok.Data;

@Data
public class JsonOptions {

    public static final JsonOptions DEFAULT = new JsonOptions(false, false);
    public static final JsonOptions DETAILED = new JsonOptions(true, false);
    public static final JsonOptions PRETTY = new JsonOptions(true, true);

    private final boolean detailed;
    private final boolean pretty;

}
