package dev.loredb.io;

import dev.loredb.schema.JsonSchema;
import dev.loredb.util.Reference;

import java.nio.CharBuffer;

public class InMemoryObjectStoreConfig implements JsonObjectStoreConfig<CharBuffer> {

    private final CharBuffer read, write;
    private JsonSchema schema = JsonSchema.empty();

    InMemoryObjectStoreConfig(int size) {
        this.write = CharBuffer.allocate(size);
        this.write.append("[]");
        this.read = write.asReadOnlyBuffer();
    }

    @Override
    public TextInput input(long pos) {
        return TextInput.wrap(read.asReadOnlyBuffer().position((int) pos));
    }

    @Override
    public CharBuffer output() {
        write.position(0);
        return write;
    }

    @Override
    public Reference<JsonSchema> schema() {
        return new Reference<>() {
            @Override
            public void set(JsonSchema ref) {
                schema = ref;
            }
            @Override
            public JsonSchema get() {
                return schema;
            }
        };
    }

    @Override
    public void after(Appendable output) {} // do nothing
}
