package dev.loredb.io;

public class IORuntimeException extends RuntimeException {
    public IORuntimeException(String message) {
        super(message);
    }
    public IORuntimeException(Throwable cause) {
        super(cause);
    }
}
