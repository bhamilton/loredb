package dev.loredb.io;

import dev.loredb.schema.JsonSchema;
import dev.loredb.util.Reference;

public interface JsonObjectStoreConfig<O extends Appendable> {

    TextInput input(long pos);

    O output();

    void after(Appendable output);

    Reference<JsonSchema> schema();
}
