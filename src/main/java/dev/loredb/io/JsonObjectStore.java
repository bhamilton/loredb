package dev.loredb.io;

import com.carrotsearch.hppc.ObjectLongIdentityHashMap;
import com.carrotsearch.hppc.ObjectLongMap;
import dev.loredb.ArrayNode;
import dev.loredb.JsonNode;
import dev.loredb.ObjectNode;
import dev.loredb.ObjectStore;
import dev.loredb.ValueNode;
import dev.loredb.ValueNode.IdNode;
import dev.loredb.index.Ids;
import dev.loredb.schema.JsonSchema;
import dev.loredb.util.Reference;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.LongFunction;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class JsonObjectStore implements ObjectStore {

    // TODO auto-expansion on buffer overflow
    public static JsonObjectStore inMemory() {
        return new JsonObjectStore(new InMemoryObjectStoreConfig(1024 * 1024));
    }

    public static JsonObjectStore fromPath(Path jsonFile) {
        return new JsonObjectStore(new FileObjectStoreConfig(jsonFile));
    }

    private final LongFunction<TextInput> source;
    private final Supplier<Appendable> sink;
    private final Consumer<Appendable> after;
    private final ObjectLongMap<IdNode> index;
    private final Reference<JsonSchema> schema;
    private final JsonParser parser;

    public <O extends Appendable> JsonObjectStore(JsonObjectStoreConfig<O> conf) {
        this(conf::input, conf::output, conf::after, conf.schema(), new JsonParser());
    }

    public JsonObjectStore(LongFunction<TextInput> source,
                           Supplier<Appendable> sink,
                           Consumer<Appendable> after,
                           Reference<JsonSchema> schema,
                           JsonParser parser) {
        this.source = source;
        this.sink = sink;
        this.after = after;
        this.schema = schema;
        this.parser = parser;
        this.index = initIndex();
    }

    private ObjectLongMap<IdNode> initIndex() {
        return new ObjectLongIdentityHashMap<>();
    }

    @Override
    public synchronized void save(Stream<ObjectNode> objects) {
        Appendable out = sink.get();
        index.clear();
        try {
            out.append("[");
            Iterator<ObjectNode> iterator = objects.iterator();
            int pos = 1;
            if (iterator.hasNext()) {
                String json = nextNodeJson(iterator, pos);
                out.append(json);
                pos += json.length() + 1;
            }
            while (iterator.hasNext()) {
                String json = nextNodeJson(iterator, pos);
                out.append(',').append(json);
                pos += json.length() + 1;
            }
            out.append("]");
        } catch (IOException e) {
            throw new IORuntimeException(e);
        } finally {
            after.accept(out);
        }
    }

    private String nextNodeJson(Iterator<ObjectNode> iterator, int pos) {
        ObjectNode node = iterator.next();
        JsonNode id = node.get(Ids.ATTR);
        if (id instanceof IdNode)
            index.put((IdNode) id, pos);
        return node.json();
    }

    @Override
    public Stream<ObjectNode> stream() {
        JsonNode node = parser.parse(source.apply(0));
        if (node instanceof ArrayNode)
            return ((ArrayNode) node).children().map(ObjectNode.class::cast);
        else if (node instanceof ValueNode.NullNode)
            return Stream.empty();

        throw new IllegalStateException("Invalid node! " + node);
    }

    @Override
    public ObjectNode get(IdNode id) {
        long pos = index.get(id);
        JsonNode node = parser.parse(source.apply(pos));
        if (node instanceof ObjectNode)
            return (ObjectNode) node;

        throw new IllegalStateException("Node does not exist for id " + id);
    }

    @Override
    public JsonSchema schema() {
        return schema.get();
    }

    @Override
    public void schema(JsonSchema s) {
        schema.set(s);
    }
}
