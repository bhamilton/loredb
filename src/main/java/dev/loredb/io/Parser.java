package dev.loredb.io;

import java.io.Reader;

public interface Parser<T> {

    default T parse(String str) {
        return parse(TextInput.wrap(str));
    }

    default T parse(Reader reader) {
        return parse(TextInput.wrap(reader));
    }

    T parse(TextInput input);

}
