package dev.loredb.io;

import dev.loredb.JsonNode;
import dev.loredb.ObjectNode;
import dev.loredb.schema.JsonSchema;
import dev.loredb.util.Reference;

import java.io.BufferedReader;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.channels.Channels;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashSet;

import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

public class FileObjectStoreConfig implements JsonObjectStoreConfig<Writer> {

    // TODO use ascii and encode utf strings json style?
    private static final Charset CHARSET = Charset.defaultCharset();

    private final SeekableByteChannel read, write;
    private final Path path;

    public FileObjectStoreConfig(Path path) {
        this.path = path;

        try {
            Path file = path.resolve("data.json");
            if (!Files.exists(path)) {
                Files.createDirectories(path);
                try (OutputStream out = Files.newOutputStream(file)) {
                    out.write("[]".getBytes());
                }
            }
            this.read = Files.newByteChannel(file, new HashSet<>(Collections.singleton(READ)));
            this.write = Files.newByteChannel(file, new HashSet<>(Collections.singleton(WRITE)));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public TextInput input(long pos) {
        try {
            return TextInput.wrap(Channels.newReader(read.position(pos), CHARSET));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public Writer output() {
        try {
            return Channels.newWriter(write.position(0), CHARSET);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public void after(Appendable output) {
        try {
            if (output instanceof Flushable)
                ((Flushable) output).flush();
            // when the json shrinks, we need to truncate
            write.truncate(write.position());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public Reference<JsonSchema> schema() {

        Path schemaFile = path.resolve("schema.json");

        return new Reference<>() {

            JsonSchema schema = Files.exists(schemaFile) ? readSchema(schemaFile) : JsonSchema.empty();

            @Override
            public void set(JsonSchema ref) {
                schema = ref;
                try (var writer = Files.newBufferedWriter(schemaFile)) {
                    writer.append(JsonNode.of(ref).json());
                } catch (IOException e) {
                    throw new IORuntimeException(e);
                }
            }
            @Override
            public JsonSchema get() {
                return schema;
            }

        };
    }

    private JsonSchema readSchema(Path file) {
        try (BufferedReader reader = Files.newBufferedReader(file)){
            JsonNode node = new JsonParser().parse(reader);
            if (node instanceof ObjectNode)
                return node.asA(JsonSchema.class);
            throw new IllegalStateException("Unexpected node type " + node.type() + ": " + node.json());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

}
