package dev.loredb;

import dev.loredb.io.JsonOptions;
import dev.loredb.schema.JsonNodeType;
import dev.loredb.util.Primitives;
import lombok.Data;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.Optional;

import static dev.loredb.index.Ids.*;

public interface ValueNode<T extends Comparable<T>> extends JsonNode, Comparable<ValueNode<T>> {

    T getValue();

    default boolean isNull() {
        return false;
    }

    default String json(JsonOptions options) {
        return String.valueOf(getValue());
    }

    @SuppressWarnings("unchecked")
    default <E> E asA(Class<E> type) {
        T value = getValue();
        if (type.isInstance(value))
            return type.cast(value);
        return (E) value; // TODO why does this even work for primitives?
    }

    default Object asA(Type type) {
        if (type instanceof Class<?>)
            return asA((Class<?>) type);
        throw new IllegalArgumentException("Cannot convert " + type() + " to " + type);
    }

    @Override
    default int compareTo(ValueNode<T> o) {
        if (o.isNull()) // TODO nulls first / last?
            return isNull() ? 0 : -1;
        else if (isNull())
            return 1;
        return getValue().compareTo(o.getValue());
    }

    @Data
    class StringNode implements ValueNode<String> {
        private final String value;
        @Override
        public JsonNodeType type() {
            return JsonNodeType.string;
        }

        @SuppressWarnings({"unchecked", "rawtypes"})
        @Override
        public <E> E asA(Class<E> type) {
            if (type.equals(String.class))
                return type.cast(value);
            else if (type.isEnum())
                return (E) Enum.valueOf((Class) type, value);

            if (type.isPrimitive())
                type = Primitives.wrap(type);
            if (Primitives.isWrapperType(type)) {
                try {
                    return (E) type.getDeclaredConstructor(String.class).newInstance(value);
                } catch (ReflectiveOperationException e) {
                    throw new IllegalArgumentException(e);
                }
            }

            throw new IllegalArgumentException("Cannot parse as " + type);
        }

        // TODO UTF
        @Override
        public String json(JsonOptions options) {
            try {
                StringBuilder sb = new StringBuilder(value.length() + 2).append('"');
                StringReader reader = new StringReader(value);
                int c;
                while ((c = reader.read()) != -1)
                    mapChar(sb, (char) c);
                return sb.append('"').toString();
            } catch (IOException e) {
                return '"' + value + '"';
            }
        }

        private void mapChar(StringBuilder sb, char c) {
            switch (c) {
                case '"': case '\\': case '/': sb.append('\\').append(c); break;
                case '\f': sb.append("\\f"); break;
                case '\n': sb.append("\\n"); break;
                case '\r': sb.append("\\r"); break;
                case '\t': sb.append("\\t"); break;
                default: sb.append(c);
            }
        }

        @Override
        public String toString() {
            return json();
        }
    }

    interface NumberNode<N extends Number & Comparable<N>> extends ValueNode<N> {

        @Override
        default JsonNodeType type() {
            return JsonNodeType.number;
        }

    }

    @Data
    class IntegerNode implements NumberNode<Integer> {
        private final Integer value;
        @Override
        public String toString() {
            return json();
        }
    }

    @Data
    class LongNode implements NumberNode<Long> {
        private final Long value;
        @Override
        public String toString() {
            return json();
        }
    }

    @Data
    class BigIntegerNode implements NumberNode<BigInteger> {
        private final BigInteger value;
        @Override
        public String toString() {
            return json();
        }
    }

    @Data
    class DoubleNode implements NumberNode<Double> {
        private final Double value;
        @Override
        public String toString() {
            return json();
        }
    }

    @Data
    class BooleanNode implements ValueNode<Boolean> {
        private final Boolean value;
        @Override
        public <E> E asA(Class<E> type) {
            if (type.equals(boolean.class))
                return type.cast(value);
            return type.cast(getValue());
        }
        @Override
        public JsonNodeType type() {
            return JsonNodeType.bool;
        }
        @Override
        public String toString() {
            return json();
        }
    }

    @Data
    class NullNode implements ValueNode<String> {

        @Override
        public String getValue() {
            return null;
        }

        @Override
        public JsonNodeType type() {
            return JsonNodeType.nil;
        }

        @Override
        public boolean isNull() {
            return true;
        }

        @Override
        public <E> E asA(Class<E> type) {
            if (Optional.class.isAssignableFrom(type))
                return type.cast(Optional.empty());
            return null;
        }

        @Override
        public Object asA(Type type) {
            if (type instanceof ParameterizedType && ((ParameterizedType) type).getRawType().equals(Optional.class))
                return Optional.empty();
            return null;
        }

        @Override
        public String json(JsonOptions options) {
            return "null";
        }

        @Override
        public String toString() {
            return json();
        }
    }

    @Data
    class TimeNode implements ValueNode<Instant> {
        private final Instant value;

        @Override
        public JsonNodeType type() {
            return JsonNodeType.time;
        }

        @Override
        public String json(JsonOptions options) {
            return '"' + value.toString() + '"';
        }
    }

    @Data
    class IdNode implements ValueNode<ByteBuffer> {
        private final ByteBuffer value;

        public IdNode(String str) {
            this(readCompressed(str));
        }
        public IdNode(ByteBuffer value) {
            this.value = value;
        }

        @Override
        public JsonNodeType type() {
            return JsonNodeType.id;
        }

        @Override
        public String json(JsonOptions options) {
            return '"' + writeCompressed(value) + '"';
        }

        public int userNumber() {
            return readUserNumber(value);
        }

        public long timestamp() {
            return readTimestamp(value);
        }

        public Instant instant() {
            return Instant.ofEpochMilli(timestamp());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof IdNode)) return false;

            IdNode idNode = (IdNode) o;

            return toString().equals(idNode.toString()); // TODO why don't my bytebuffers equal...
        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        public String toString() {
            return writeCompressed(value);
        }

    }

}
