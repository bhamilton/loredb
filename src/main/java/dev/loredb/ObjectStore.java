package dev.loredb;

import dev.loredb.ValueNode.IdNode;
import dev.loredb.schema.JsonSchema;

import java.util.stream.Stream;

public interface ObjectStore {

    JsonSchema schema();

    void schema(JsonSchema schema);

    /**
     * Read node for given ID.
     */
    ObjectNode get(IdNode id);

    /**
     * Read all nodes.
     */
    Stream<ObjectNode> stream();

    /**
     * Write over all nodes.
     */
    void save(Stream<ObjectNode> objects);

}
