# LoreDB

An embedded database for quick development in JVM languages.  The database maintains changes in-memory and 
saves to disk asynchronously, so changes are lightning fast.

### Features

1. Complete logging of changes for time travel and auditing.
2. Data saved to JSON files for easy external processing.
3. JSON schema for data consistency and validation.
4. Pub-sub for real-time services.
5. Reflection.

Coming...

- Indexing, joins, references
- REST API, auth

## How to use

Create an instance:

```
import io.lore.Database;

// create a json database rooted in the given folder
Database db = Database.fromPath("/opt/data");

// create an in-memory instance, nothing is saved to disk
Database db = Database.inMemory();
```

Create, read, update and delete:

```
import static io.lore.criteria.Comparison.*;

LoreTable books = db.table("books");

// insert an entry
books.insert("""
  {
    "isbn": "9781491904244",
    "title": "You Don't Know JS",
    "published": "2015-12-27T00:00:00.000Z",
    "publisher": "O'Reilly Media",
    "pages": 278
  }
""");

// print out titles
books.read().map(n -> n.get("title")).forEach(System.out::println);

// update an entry
books.update(eq("isbn", 9781491904244), book -> book.set("publisher", "Penguin Classics"));

// delete entries
books.delete(greaterThan("pages", 300));
```
